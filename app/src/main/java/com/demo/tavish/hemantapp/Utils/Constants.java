package com.demo.tavish.hemantapp.Utils;

public class Constants {

    public static final int STORAGE_PERMISSION_CODE = 1;
    public static final String BASE_URL="http://13.127.153.30:80/soul_wings/webapi/";
    public static final String DOWNLOAD_URL="http://13.127.153.30:80/soul_wings/";
  /* public static final String BASE_URL="http://192.168.0.109:8080/soul_wings/webapi/";
    public static final String DOWNLOAD_URL="http://192.168.0.109:8080/soul_wings/";*/
    public static final String KEY_USER_NAME = "user_id";
    public static final String LOGIN_SUCCESSFUL="login_successful";
    public static final String SHARED_PREFERENCE = "soul_wings_prefs";
    public static final String DEFAULT_PREFERENCE = "Default User";
    public static final String ADMIN_RIGHTS = "(admin)";
    public static final String SOUL_WINGS_OWNER="Heman1109";
   // public static final String SOUL_WINGS_OWNER="tavish";
    public static final String USER_RIGHTS = "(user)";

    public static final int SPLASH_TIME_OUT = 2000;
}
