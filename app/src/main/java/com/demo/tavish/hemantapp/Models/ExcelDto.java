package com.demo.tavish.hemantapp.Models;

public class ExcelDto {

    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
