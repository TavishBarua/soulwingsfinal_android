package com.demo.tavish.hemantapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.tavish.hemantapp.Interface.ApiInterface;
import com.demo.tavish.hemantapp.Models.UserDto;
import com.demo.tavish.hemantapp.Utils.Constants;
import com.demo.tavish.hemantapp.Utils.RetroResponse.ApiClient;
import com.demo.tavish.hemantapp.Utils.RetroResponse.ApiResponse;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    private SharedPreferences sharedPreferences;
    private String str_userName;
    TextView tv_userName, tv_rights;
    LinearLayout ll_content_main;
    Button btn_create;
    EditText et_create_username, et_create_password;
    TextView tv_main_name, tv_main_password, tv_welcome_text;
    CardView card_main_two;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        tv_userName = (TextView) header.findViewById(R.id.tv_userName);
        tv_rights = (TextView) header.findViewById(R.id.tv_rights);

        tv_main_name = (TextView) findViewById(R.id.tv_main_name);
        tv_main_password = (TextView) findViewById(R.id.tv_main_password);
        tv_welcome_text = (TextView) findViewById(R.id.tv_welcome_text);

        et_create_username = (EditText)  findViewById(R.id.et_create_username);
        et_create_password = (EditText) findViewById(R.id.et_create_password);

        btn_create = (Button) findViewById(R.id.btn_create);
        btn_create.setOnClickListener(this);

        card_main_two = (CardView) findViewById(R.id.main_card_two);



        ll_content_main = (LinearLayout) findViewById(R.id.ll_content_main);

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCE, MODE_PRIVATE);
        str_userName = sharedPreferences.getString(Constants.KEY_USER_NAME," ");



        tv_userName.setText(str_userName);
        if (tv_userName.getText().toString().equals(Constants.SOUL_WINGS_OWNER)){
            tv_rights.setText(Constants.ADMIN_RIGHTS);
            ll_content_main.setVisibility(View.VISIBLE);
            tv_welcome_text.setVisibility(View.INVISIBLE);

        }else{
            tv_welcome_text.setText("Welcome  "+str_userName);
            tv_rights.setText(Constants.USER_RIGHTS);
            tv_welcome_text.setVisibility(View.VISIBLE);
            ll_content_main.setVisibility(View.INVISIBLE);
        }


       /* if (sharedPreferences.getString(Constants.KEY_USER_NAME,Constants.DEFAULT_PREFERENCE).equals(Constants.LOGIN_SUCCESSFUL)){
        tv_userName.setText(Constants.KEY_USER_NAME);
        tv_rights.setText(Constants.ADMIN_RIGHTS);
        }*/



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_create:
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(card_main_two.getWindowToken(), 0);
                createUser();
                break;

        }

    }

    private void createUser(){
        final UserDto userDto = new UserDto();
        userDto.setUserName(et_create_username.getText().toString());
        userDto.setPassword(et_create_password.getText().toString());
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMax(100);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        Call<ApiResponse<UserDto>> call = apiInterface.user_create(userDto);
        call.enqueue(new Callback<ApiResponse<UserDto>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserDto>> call, Response<ApiResponse<UserDto>> response) {
                try{
                    if (response.isSuccessful()){
                        progressDialog.dismiss();
                        if (response.body().getStatus()){
                            card_main_two.setVisibility(View.VISIBLE);
                            UserDto userDto1 = response.body().getObject().get(0);
                            tv_main_name.setText(userDto1.getUserName());
                            tv_main_password.setText(userDto1.getPassword());
                            snackBarMessage("User Successfully Created");
                        }else {
                            card_main_two.setVisibility(View.INVISIBLE);
                            snackBarMessage("UserName Already Exists");
                        }


                    }else {
                        progressDialog.dismiss();
                        snackBarMessage("Something Went Wrong");
                    }

                }catch (Exception e){
                        e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserDto>> call, Throwable t) {
                    progressDialog.dismiss();
                    snackBarMessage("Server Error");
            }
        });

    }








    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
           return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_inventory) {
            // Handle the camera action

                Intent i = new Intent(MainActivity.this, InventoryActivity.class);
                startActivity(i);

        }
        else if (id==R.id.nav_sale_data){

            if(tv_rights.getText().toString().equals(Constants.ADMIN_RIGHTS)) {
            Intent i = new Intent(MainActivity.this, CalculateSale.class);
            startActivity(i);
            }else{
                snackBarMessage("Sorry, You Don't have admin Rights.");
            }
        }
        else if (id==R.id.nav_logout){
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCE, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
            Toast.makeText(this, "You've Been Successfully Logged Out", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void snackBarMessage(String message){
        Snackbar mSnackBar = Snackbar.make(findViewById(android.R.id.content),message, Snackbar.LENGTH_LONG);
        TextView tv = (mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        tv.setTypeface(Typeface.createFromAsset(
                getAssets(),
                "fonts/Lato-Regular.ttf"));
        mSnackBar.show();
    }


}
