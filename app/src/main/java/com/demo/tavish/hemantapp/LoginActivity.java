package com.demo.tavish.hemantapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.demo.tavish.hemantapp.Interface.ApiInterface;
import com.demo.tavish.hemantapp.Models.UserDto;
import com.demo.tavish.hemantapp.Utils.Constants;
import com.demo.tavish.hemantapp.Utils.RetroResponse.ApiClient;
import com.demo.tavish.hemantapp.Utils.RetroResponse.ApiResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{


    EditText et_login_username, et_login_password;
    Button btn_login;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreference;
    String str_preference="intent_username";
    String str_username, str_password;
    CardView loginCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_login_username = (EditText) findViewById(R.id.et_login_username);
        et_login_password = (EditText) findViewById(R.id.et_login_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        loginCard = (CardView) findViewById(R.id.login_card);


        sharedPreference = getSharedPreferences(Constants.SHARED_PREFERENCE, MODE_PRIVATE);
        if (sharedPreference.getString(str_preference," ").equals(Constants.LOGIN_SUCCESSFUL)){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }


    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        switch (v.getId()){

            case R.id.btn_login:
                imm.hideSoftInputFromWindow(loginCard.getWindowToken(), 0);
                   str_username = et_login_username.getText().toString();
                   str_password = et_login_password.getText().toString();
                   if(et_login_username.getText().toString().trim().length()>0
                           && et_login_password.getText().toString().trim().length()>0){
                       userLogin();
                   }else{
                       snackBarMessage("UserName or Password is Missing");
                   }


        }

    }

    private void userLogin(){

        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        final UserDto userDto = new UserDto();
        userDto.setUserName(et_login_username.getText().toString());
        userDto.setPassword(et_login_password.getText().toString());

        Call<ApiResponse<UserDto>> call = apiInterface.user_login(userDto.getUserName(), userDto.getPassword());
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMax(100);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // show it
        progressDialog.show();
        call.enqueue(new Callback<ApiResponse<UserDto>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserDto>> call, Response<ApiResponse<UserDto>> response) {
                try{
                    if (response.isSuccessful()){
                        progressDialog.dismiss();
                        if (response.body().getStatus()){
                            UserDto userDto1 = response.body().getObject().get(0);
                            editor = sharedPreference.edit();
                            editor.putString(Constants.KEY_USER_NAME, userDto1.getUserName());
                            editor.putString(str_preference,Constants.LOGIN_SUCCESSFUL);
                            editor.commit();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            snackBarMessage("User Or Password is Incorrect");
                        }
                    }else{
                        progressDialog.dismiss();
                        snackBarMessage("Something Went Wrong");
                    }


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserDto>> call, Throwable t) {
                progressDialog.dismiss();
                snackBarMessage("Server Error");
            }
        });

    }


    private void snackBarMessage(String message){
        Snackbar mSnackBar = Snackbar.make(findViewById(android.R.id.content),message, Snackbar.LENGTH_LONG);
        TextView tv = (mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        tv.setTypeface(Typeface.createFromAsset(
                getAssets(),
                "fonts/Lato-Regular.ttf"));
        mSnackBar.show();
    }

}
